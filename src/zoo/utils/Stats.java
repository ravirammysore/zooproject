package zoo.utils;

public class Stats {
	
	private static int totalAnimals;
	
	public static void incrementAnimalCount() {
		totalAnimals++;
		updateTheCountInDatabase();
	}
		
	public static void incrementAnimalCount(int byHowMuch) {
		totalAnimals = totalAnimals + byHowMuch;
		updateTheCountInDatabase();
	}
	
	private static void updateTheCountInDatabase() {
		//logic for writing to database
		System.out.println("Updated database!");
	}
}

package zoo;

import java.text.MessageFormat;

import zoo.animals.*;
import zoo.utils.Stats;

public class Main {

	public static void main(String[] args) {
		System.out.println("\n---------Welcome to Zoo Game!------\n"); //Ravi old			

		Animal[] animals = new Animal[6]; //from eclipse
		
		//cannot instantiate abstract class
		//Animal a = new Animal("Someone");
		
		animals[0] = new Dog("Doggy");
		animals[1] = new Cat("Tom");		
		animals[2] = new Monkey("Kapish");		
		animals[3] = new Peacock("Morni");
		animals[4] = new Parrot("Kikki");
		animals[5] = new KingFisher("Fisher");		
		
		//Class.method
		Stats.incrementAnimalCount(6);		
		
		for(Animal animal:animals)
			greetTheUser(animal);
		
		//for now
		var stage = 1; 		
		
		Cat tom = (Cat) animals[1];
		
		tom.climb();
		tom.tellName();		
		//tom._name = "Sam";  violation of encapsulation principle, so error
		
		tom.set_health(10);		
		//no effect	
		tom.set_health(-555); 
		var name = tom.get_name(); 		
		
		var msg1 = MessageFormat.format("Player 1, you will be ''{0}'' for stage {1} of the game!", name, stage);		
		System.out.println(msg1);
		
		var msg2 = MessageFormat.format("He is cute and right now his health is {0}",tom.get_health());		
		System.out.println(msg2);
		
		System.out.println("\n----more to come soon!----\n");
	}
	
	public static void greetTheUser(Animal animal) {
		
		//object.method
		animal.tellName();
		animal.jump();
		animal.climb();
			
					
		if(animal instanceof Bird) {			
			Bird b = (Bird)animal;		
			b.fly();
		}
		
		if(animal instanceof ISingable) {
			ISingable a = (ISingable)animal;
			a.sing();
		}
		
		if(animal instanceof IDanceable) {
			IDanceable a = (IDanceable)animal;
			a.dance();
		}	
	}				
}

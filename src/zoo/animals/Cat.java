package zoo.animals;

public class Cat extends Animal {
	
	public Cat(String name) {
		super(name);
	}	
	
	@Override
	public void climb() {
		System.out.println(_name + " is climbing quickly...");
	}
}

package zoo.animals;

public class Dog extends Animal {	
	
	public Dog(String name) {
		super(name);
	}	
	
	@Override
	public void climb() {
		System.out.println(_name + " is climbing slowly...");
	}
}

package zoo.animals;

public class KingFisher extends Bird {

	public KingFisher(String name) {
		super(name);
	}

	@Override
	public void climb() {
		System.out.println(_name + " is climbing swiftly...");
	}
	
	@Override
	public void fly() {
		System.out.println(_name + " is flying fast...");
	}
}

package zoo.animals;

public abstract class Animal {
	
	String _name;
	int _health;
	
	int _age=0;
	
	//public, protected, private (default) - Visibility Modifiers 
	public Animal(String name) {
		_name = name;
	}
	
	public void tellName() {
		System.out.println("My name is " + _name + "!");
	}
	
	public void jump() {
		System.out.println(_name + " is jumping...");
	}
	
	abstract public void climb();

	//Getter (accessor)
	public String get_name() {
		return _name;
	}

	public int get_health() {
		return _health;
	}

	public void set_health(int _health) {
		if(_health>=0 && _health <=10)
			this._health = _health;		
	} 		
}

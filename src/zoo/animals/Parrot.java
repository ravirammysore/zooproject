package zoo.animals;

public class Parrot extends Bird implements ISingable {	
	
	public Parrot(String name) {
		super(name);
	}
		
	@Override
	public void climb() {
		System.out.println(_name + " is climbing sweetly...");
	}
	
	@Override
	public void fly() {
		System.out.println(_name + " is flying quickly...");
		
	}
	
	@Override
	public void sing() {
		System.out.println(_name + " is singing melodiously...");
	}
}

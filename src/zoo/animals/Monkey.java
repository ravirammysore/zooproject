package zoo.animals;

public class Monkey extends Animal {	
	
	public Monkey(String name) {
		super(name);
	}	
	
	@Override
	public void climb() {
		System.out.println(_name + " is climbing blazing fast...");
	}
}

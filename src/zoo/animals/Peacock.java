package zoo.animals;

public class Peacock extends Bird implements ISingable, IDanceable {	
	
	public Peacock(String name) {
		super(name);
	}
		
	@Override
	public void climb() {
		System.out.println(_name + " is climbing carefully...");
	}
	@Override
	public void fly() {
		System.out.println(_name + " is flying heavily...");
	}

	@Override
	public void sing() {
		System.out.println(_name + " is singining nicely...");
	}

	@Override
	public void dance() {
		System.out.println(_name + " is dancing graciously...");
		
	}
}

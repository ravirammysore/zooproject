package zoo.animals;

//An Interface is a pure abstract class
public interface ISingable {
	void sing();
}
package zoo.animals;

public interface IDanceable {
	void dance();
}